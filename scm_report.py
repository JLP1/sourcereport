#!/usr/bin/env python

from github import Github
from bitbucket.bitbucket import Bitbucket

# custom classes
from Utilities import Colorize
from LoginParamsConf import AccessRemoteSCM

# Purpose: 
# Access remote git accounts and generate list of repos.
#   * Github
#   * Bitbucket

# Output: 
# List of repos for each account, displays:
#   * pivate/public,
#   * description,
#   * last updated date-time. 

class SourceReport(object):
    
    # load remote account access params
    cred_github = AccessRemoteSCM.access_github()
    cred_bb = AccessRemoteSCM.access_bitbucket()

    # simple border
    border = '.' * 110

    # Display mode limits the repo display loops:
    # 'preview' is for demo purpose, to show only a limited list
    display_options = {
    'all': 'show all repositories',
    'preview': 'show limited count of repos'
    }

    # set the display mode ( 'all' / 'preview' )
    display_mode = 'preview'

    # limit the items to display under preview mode
    preview_max  = 4

    # - Main report process
    @staticmethod
    def display_report():

        # get display mode info
        display_notes = SourceReport.display_options[SourceReport.display_mode]

        # if preview mode, show max count
        if SourceReport.display_mode == 'preview':
            display_notes += ', max = ({})'.format(
                Colorize.go(SourceReport.preview_max, 'red')
                )

        # intro, indicate display mode
        print('{}\nSource Report: Display mode = {}: {}').format(
            Colorize.go( SourceReport.border, 'blue'),
            Colorize.go(SourceReport.display_mode,'green'),
            display_notes
            )
        
        # github report
        SourceReport.show_github()

        # bitbucket report
        SourceReport.show_bitbucket()

        # conclusion
        print '\ndone.\n'

    # - GITHUB
    @staticmethod
    def show_github():

        g = Github(
            SourceReport.cred_github['user'], 
            SourceReport.cred_github['pass']
            )
        ghuser = g.get_user()
        my_repos = ghuser.get_repos()

        # convert to list to easily get len
        repo_list = list(my_repos)

        print('{}\n> Github: User: {}, Repos: {}'.format(
            Colorize.go( SourceReport.border, 'blue'),
            Colorize.go( SourceReport.cred_github['user'] , 'green'),
            Colorize.go(len(repo_list), 'yellow')
            )
        )

        owner_name = SourceReport.cred_github['user']

        # loop over repos
        for gh_idx, repo in enumerate(my_repos):
            
            # if preview mode, limit display
            if SourceReport.display_mode == 'preview':
                if (gh_idx + 1) > SourceReport.preview_max:
                    break

            # get owner of repo - example if we didnt know the owner:
            # this line checks the repo owner and slows things down.
            # owner_name = repo._owner.value.name

            # set colors for repo visibility indicator
            p = Colorize.go('+', 'green')
            if repo.private == 1:
                p = Colorize.go('-', 'red')

            # output repo summary
            print '\033[1;34m# {:>3}.\033[1;m  {} {:<20} {:<72} {}'.format(
                (gh_idx + 1),
                p,
                repo.name, 
                Colorize.go(repo.description,'blue'), 
                Colorize.go(repo.updated_at,'cyan')
                )

            # > Learn more about the repo object: show all methods
            # for mthd in dir(repo):
            #     # print mthd + ' : ' repo.mthd()
            #     print mthd + ' : ' 
            #     print '---------------------'

            #
            # owned_private_repos

    # - BITBUCKET
    @staticmethod
    def show_bitbucket():

        bb = Bitbucket(
            SourceReport.cred_bb['user'], 
            SourceReport.cred_bb['pass']
            )

        # > Learn more about the bitbucket class: show all methods
        # for mthd in dir(bb):
        #     print mthd + ' : ' 
        #     print '---------------------'

        success, repositories = bb.repository.all()

        print('{}\n> Bitbucket: User: {}, Repos: {}'.format(
            Colorize.go( SourceReport.border, 'blue'),
            Colorize.go( bb.username , 'green'),
            Colorize.go(len(repositories), 'yellow')
            )
        )

        # loop over repos
        for bb_idx, repo in enumerate(sorted(repositories)):

            # if preview mode, limit display
            if SourceReport.display_mode == 'preview':
                if (bb_idx + 1) > SourceReport.preview_max:
                    break

            # set colors for repo visibility indicator
            p = Colorize.go( '+', 'green')
            if repo['is_private']:
                p = Colorize.go( '-', 'red')
            
            update_color = 'cyan'
            
            # output repo summary
            print('\033[1;34m# {:>3}.\033[1;m  {} {:<20} {:<70}   {}'.format(
                (bb_idx + 1), 
                p, 
                repo['name'], 
                Colorize.go( repo['description'], 'blue'), 
                Colorize.go( repo['last_updated'], update_color)
                #repo['scm']
                )
            )
# end class

# run the report via class static method
SourceReport.display_report()

# end