# General Utilities

# Help keep syntax cleaner by keeping all the noisy color
# control codes encapsulated here rather than throughout the rest of the code.
class Colorize(object):

    COLOR_MAP = {
        "red": "31",
        "green": "32",
        "yellow": "33",
        "blue": "34",
        "magenta": "35",
        "cyan": "36"
    }

    @staticmethod
    def go(str, color):
        return "\033[1;{0}m{1}\033[1;m".format(Colorize.COLOR_MAP[color], str)
# end of ColorString class

