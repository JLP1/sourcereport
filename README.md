SOURCE REPORT
=============

This small program uses simple python and OOP to login into 3rd-party SCM hosts to generate a report of the repositories for a given user account.

   * Github
   * Bitbucket

Outputs a color-coded list of repos for each account, displays:

   * private/public,
   * description,
   * last updated date-time. 



![scm_report_screen_sample.png](https://bitbucket.org/repo/LnnxpK/images/3333143984-scm_report_screen_sample.png)


Here are the links to the python projects used for the 3rd-party API connections:

• GitHub:

Project Source:
https://github.com/jacquev6/PyGithub

Documentation:
http://jacquev6.github.io/PyGithub/v1/introduction.html


• BitBucket:

Project Source:
https://github.com/Sheeprider/BitBucket-api

Documentation:
http://bitbucket-api.readthedocs.org/en/latest/installation.html