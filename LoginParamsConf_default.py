# SCM Manager remote repo account login params

# 1) specify proper user/pass credentials
# 2) rename file: remove '_default'
# new file 'LoginParamsConf.py'
class AccessRemoteSCM(object):

    @staticmethod
    def access_github():

        github = {
        'user': "my_user_github", 
        'pass': "my_pass_github"
        }
        return github

    @staticmethod
    def access_bitbucket():

        bitbucket = {
        'user': "my_user_bb", 
        'pass': "my_pass_bb"
        }
        return bitbucket
# end of AccessRemoteSCM class
